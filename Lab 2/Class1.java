package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by svetka on 4/7/2016.
 */
public class Class1 {

    public static void main(String[] args) throws IOException {

       //1
        int a = 2;
        int b = 3;
        boolean c = true;
        boolean d = false;

        System.out.println("1a. "+a+"+"+b+"="+(a+b));
        System.out.println("1b. "+a+"<="+b+"="+(a<=b));
        System.out.println("1c. "+c+"||"+d+"="+(c||d));
        a+= b;
        System.out.println("1d. a="+a+"\n");

        //2

        int valInt = 2;
        long valLong = 100500L;
        float valFloat = 0.05F;
        double valDouble = 7.8;
        boolean valBool = true;
        char valChar = 'c';
        String sInt = Integer.toString(valInt);
        String sLong = Long.toString(valLong);
        String sFloat = Float.toString(valFloat);
        String sDouble = Double.toString(valChar);
        String sBool = Boolean.toString(valBool);
        String sChar = Character.toString(valChar);

        System.out.println("2. ToString \n"+sInt+"\n"+sLong+"\n"
                +sFloat+"\n"+sDouble+"\n"+sBool+"\n"+sChar);

        int valInt1 = Integer.valueOf(sInt);
        long valLong1 = Long.valueOf(sLong);
        float valFloat1 = Float.valueOf(sFloat);
        double valDouble1 = Double.valueOf(sDouble);
        boolean valBool1 = Boolean.valueOf(sBool);
        char valChar1 = sChar.charAt(0);

        System.out.println("\nFrom String \n"+valInt1+"\n"+valLong1+"\n"
                +valFloat1+"\n"+valDouble1+"\n"+valBool1+"\n"+valChar1);




        //3

        valInt = (int) valDouble;
        valFloat = (float) valInt;

        System.out.println("\n3. \nDouble to Int "+valInt+"\nInt to Float "+valFloat+"\n");

        //4


        double var1 = 0;
        double var2 = 0;
        String s1 = "", s2 = "", s3 = "";
        BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Type argument #1");

        s1 = br.readLine();

        System.out.println("Type sign of math operation");

        s2 = br.readLine();

        System.out.println("Type argument #2");

        s3 = br.readLine();
        boolean b1 = false, b2 = false;

        try {
            Double.parseDouble(s1);
            b1 = true;
        } catch (Exception e) {
            b1 = false;
        }

        try {
            Double.parseDouble(s3);
            b2 = true;
        } catch (Exception e) {
            b2  =false;
        }


        if (b1&&b2) {


            var1 = Double.valueOf(s1);
            var2 = Double.valueOf(s3);

            switch (s2) {

                case "+": {
                    System.out.println("Result: " + (var1 + var2));
                    break;
                }

                case "-": {

                    System.out.println("Result: " + (var1 - var2));
                    break;
                }

                case "*": {

                    System.out.println("Result: " + (var1 * var2));
                    break;
                }

                case "/": {

                    System.out.println("Result: " + (var1 / var2));
                    break;
                }

                case "%": {
                    System.out.println("Result: " + (var1 % var2));
                    break;
                }

                default: {
                    System.out.println("Character you have entered is not sign of math operation");
                }
            }

        }
        else {System.out.println("One of arguments you entered is not a number");}


















    }
}
