/**
 * Created by Svetka on 09.04.2016.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Lab_3_1 {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


        String s = "";


        System.out.println("Enter 4 integer numbers separating them by spaces");
        s = br.readLine();

        String[] sMas = s.split(" ");
        boolean  b = true;


        int[] mas = new int[sMas.length];
        for (int i = 0; i < sMas.length; i++) {
            try {
                mas[i] = Integer.valueOf(sMas[i]);
            }
            catch (Exception e) {
                b = false;
            }
        }

        if (b == false){

            System.out.println("Data you entered is not correct");
        }
        else {

        int min = mas[0];

        for(int i=1;i< mas.length;i++){

            if(mas[i]<min){

                min = mas[i];

            }
        }
            System.out.println("The minimum number from the array is: "+min);
        }


    }
}
