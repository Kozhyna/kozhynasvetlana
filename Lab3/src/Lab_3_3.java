/**
 * Created by Svetka on 09.04.2016.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab_3_3 {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String sNum = "";
        boolean b  = true;
        int num = 0;
        System.out.println("Type any integer number greater than 1:");
        sNum = br.readLine();



        try {
            num = Integer.valueOf(sNum);
        }
        catch (Exception e){
            b = false;
                    }

        if(num<2){
            b = false;
        }

        if (b==false){

            System.out.println("Data you entered is not correct");
        }

        else{
            System.out.println("All prime numbers from 2 to entered number are the next: \n2");
            int remainder = 1;
            for (int i = 3; i <= num; i+=2) {

                for (int j = 3; j < i; j+=2)

                {

                    remainder = i%j;

                    if (remainder == 0){
                        break;
                    }

                }

                if (remainder!=0){

                    System.out.println(i+" ");
                }


            }
        }



    }
}
