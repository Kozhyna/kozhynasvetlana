import java.io.BufferedReader;

/**
 * Created by Svetka on 10.04.2016.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab_3_5 {

    public static void main(String[] args) throws IOException {

        double sum = 0;
        String s = "0";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Please type numbers separating them by 'Enter' and then type word 'Сумма'");


        try {

            while (s.equals("Сумма") == false) {
                sum += Double.valueOf(s);
                s = br.readLine();
            }


            if (sum % 1 == 0) {

                System.out.printf("Sum of entered numbers is " + (int) sum);
            } else {

                System.out.printf("Sum of entered numbers is " + sum);

            }
        }

        catch (Exception e)
        {
            System.out.println("Data you entered is incorrect.");
        }
    }
}
