/**
 * Created by Svetka on 13.04.2016.
 */
public class Auxiliary {


    public static double VFromKMperHourToMperSecond(double speed){

        speed = speed*10/36;

        return speed;
    }

    public static double[] FillTimeArray(Car[] mas){
        double[] array = new double[mas.length];

        for(int i = 0;i<mas.length;i++){

            array[i] = mas[i].Race();
        }

        return array;

    }


    public static int[] SortArray(double[] mas){

        int[] numbers = new int[mas.length];
        for(int i=0;i<mas.length;i++){
            numbers[i]=i;
        }

        for(int i=mas.length-1;i>0;i--){

            for(int j=0;j<i;j++) {

                if (mas[j] > mas[j + 1]) {

                    double tmp = mas[j];
                    mas[j] = mas[j + 1];
                    mas[j + 1] = tmp;
                    int tmpInt = numbers[j];
                    numbers[j]=numbers[j+1];
                    numbers[j+1]=tmpInt;
                }


            }

        }

        return numbers;
    }


    public static void PrintArray(double[] masTime, int[] masNum){

        System.out.println("Race has finished with the next result: \n");

        for(int i=0;i<masTime.length;i++){

            System.out.println("Auto #"+(masNum[i]+1)+" has passed the track in "+(int)(masTime[i]/60)+" minute(s) "+Math.round(masTime[i]%60)+" second(s).");
        }
    }

}
