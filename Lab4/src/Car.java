/**
 * Created by Svetka on 13.04.2016.
 */


public abstract class Car extends Auxiliary {

    double maxV;
    double acceleration;
    double maneuver;
    double startV;
    double v;
    double time;



    public void StraightAcceleratedMov(double acc) {
        double s1=0, s=2000;
        v = Math.sqrt(Math.pow(startV, 2) + 2*s * acc);
        if(v>maxV){

            s1 = (Math.pow(maxV,2)- Math.pow(startV,2))/(2*acc);
            time += (maxV - startV) / acc;
            time += (s-s1) / maxV;
            v=maxV;
        }
        else {
            time += (v - startV) / acc;

        }

    }

    public void StraightMov() {

        time += 2000 / startV;

    }

    public double Race() {

        maxV = Auxiliary.VFromKMperHourToMperSecond(maxV);


        for (int i = 1; i <= 20; i++) {

            if (startV < maxV) {

                this.StraightAcceleratedMov(acceleration);
            } else {
                this.StraightMov();
            }


            if (i < 20) {
                this.Turn();

            }

        }

        return this.time;

    }

    public abstract int Turn();

}