/**
 * Created by Svetka on 13.04.2016.
 */
public class Car1 extends Car {

    public Car1(double max, double a, double m){

        maxV = max;
        acceleration = a;
        maneuver = m;
        startV = 0;
        time = 0;
        v = 0;
    }

    public int Turn (){

        if(v > maxV/2) {

            startV = v * (maneuver + (maxV / 2 - v) * 0.005);
        }
        else{
            startV = v*maneuver;
        }
        if(startV<0){
            startV=0;}

        return 1;

    }
}
