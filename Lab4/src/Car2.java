/**
 * Created by Svetka on 13.04.2016.
 */
public class Car2 extends Car {

    public Car2(double max, double a, double m){

        maxV = max;
        acceleration = a;
        maneuver = m;
        startV = 0;
        time = 0;
        v = 0;
    }


    public int Turn (){

        startV = v*maneuver;
        if(startV<0){
            startV=0;}

        if(startV < maxV/2){

            return 2;
        }

        else{
            return 1;
        }
    }

    public double Race() {

        int k=1;
        this.maxV = Auxiliary.VFromKMperHourToMperSecond(maxV);

        for (int i = 1; i <= 20; i++) {

            if (startV < maxV) {

                this.StraightAcceleratedMov(k*acceleration);
            } else {
                this.StraightMov();
            }


            if (i < 20) {
                k = this.Turn();

            }

        }
        return this.time;
    }
}
