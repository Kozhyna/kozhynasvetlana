/**
 * Created by Svetka on 14.04.2016.
 */


public class MainClass {

    public static void main(String[] args){

        Car[] carsArray = new Car[6];
        double [] timeArray = new double[carsArray.length];
        int [] numArray = new int[carsArray.length];

        carsArray[0] = new Car1(300.00, 1.5, 0.5);
        carsArray[1] = new Car2(300.00, 1.5, 0.5);
        carsArray[2] = new Car3(300.00, 1.5, 0.5);
        carsArray[3] = new Car1(200.00, 1.4, 0.4);
        carsArray[4] = new Car2(100.00, 0.8, 0.6);
        carsArray[5] = new Car3(150.5, 1.2, 0.1);

        timeArray = Auxiliary.FillTimeArray(carsArray);
        numArray = Auxiliary.SortArray(timeArray);
        Auxiliary.PrintArray(timeArray, numArray);













    }
}
