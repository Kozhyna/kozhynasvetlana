/**
 * Created by Svetka on 24.04.2016.
 */
public class WeekendException extends RuntimeException {

    public WeekendException() {

        super("Выходной день");
    }
}
