import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by Svetka on 28.04.2016.
 */
public class CopyFiles extends Thread{

    private String stroka;
    private int index;

    public void Copying(String str, int i){

        this.stroka = str;
        this.index = i;
    }
@Override
    public void run(){
        Path source = Paths.get("D:\\TestFolder");
        try {

          Files.walkFileTree(source, new MyFileVisitor());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public class MyFileVisitor extends SimpleFileVisitor {




        @Override
        public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws IOException {

            Path source = Paths.get("D:\\TestFolder");
            Path p = (Path) file;
            File f = p.toFile();
            String tmpS = source.toString();
            tmpS+=Integer.toString(index);

            Path target = Paths.get(tmpS);

            if(f.getName().contains(stroka)){

                if(Files.notExists(target)){
                    Files.createDirectories(target);
                }
                target= target.resolve(f.getName());
                System.out.println(target);
                Files.copy(p, target, StandardCopyOption.REPLACE_EXISTING);

            }
            return FileVisitResult.CONTINUE;

        }
    }

}

