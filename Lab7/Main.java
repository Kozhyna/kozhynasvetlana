import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Svetka on 03.05.2016.
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        AutoChild car1  = new AutoChild(2.4, 3000);
        CreateDirFile();
        MySerialize(car1);
        car1 = MyDeserialize();
        Class cl1 = car1.getClass();
        Class cl2 = cl1.getSuperclass();
        Method met1 = cl2.getDeclaredMethod("Move");
        met1.setAccessible(true);
        met1.invoke((Auto)car1);
        Field acc = cl2.getDeclaredField("acceleration");
        acc.setAccessible(true);
        Field s = cl2.getDeclaredField("s");
        s.setAccessible(true);
        Field t = cl2.getDeclaredField("time");
        t.setAccessible(true);
        Field speed = cl2.getDeclaredField("v");
        speed.setAccessible(true);
        double accValue  = acc.getDouble(car1);
        int sValue =s.getInt(car1);
        double tValue = t.getDouble(car1);
        double speedValue  = speed.getDouble(car1);
        System.out.println(accValue+" "+sValue+ " "+tValue+" "+speedValue);




    }

    public static AutoChild MyDeserialize() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream("D:\\TestLab7/Serialize.ser");
        ObjectInputStream ois = new ObjectInputStream(fis);
        AutoChild car = (AutoChild) ois.readObject();
        ois.close();
        fis.close();
        return car;
            }

    public static void MySerialize(AutoChild car) throws IOException {
        FileOutputStream fos = new FileOutputStream("D:\\TestLab7/Serialize.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(car);
        oos.close();
        fos.close();
    }

    private static void CreateDirFile() throws IOException {
        File file = new File("D:\\TestLab7");
        file.mkdirs();
        File file1 = new File("D:\\TestLab7/Serialize.ser");
        file1.createNewFile();
    }

}
