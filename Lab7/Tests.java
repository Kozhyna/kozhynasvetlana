/**
 * Created by Svetka on 04.05.2016.
 */
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Tests {


    @Test
    public void TestConstructor() throws NoSuchFieldException, IllegalAccessException {
        double acc = 3;
        int s = 2500;
        AutoChild auto1 = new AutoChild(acc, s);
        Class cl1 = auto1.getClass();
        Class cl2 = cl1.getSuperclass();
        Field acc1 = cl2.getDeclaredField("acceleration");
        acc1.setAccessible(true);
        Field s1 = cl2.getDeclaredField("s");
        s1.setAccessible(true);
        double accValue1 = acc1.getDouble(auto1);
        int sValue1 = s1.getInt(auto1);
        Assert.assertTrue(accValue1 == 3 && sValue1 == 2500);
    }

    @Test
    public void TestMethodMoveValidData() throws InvocationTargetException, IllegalAccessException, NoSuchFieldException, NoSuchMethodException {
        AutoChild auto11 = new AutoChild(3, 3000);
        Class cl11 = auto11.getClass();
        Class cl12 = cl11.getSuperclass();
        Method met11 = cl12.getDeclaredMethod("Move");
        met11.setAccessible(true);
        met11.invoke(auto11);
        Field speed1 = cl12.getDeclaredField("v");
        speed1.setAccessible(true);
        double speedValue11 = speed1.getDouble(auto11);
        Assert.assertTrue(speedValue11 == Math.sqrt(2 * 3 * 3000));
    }

    @Test
    public void TestMethodMoveInvalidData() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        AutoChild auto12 = new AutoChild(-1, -1);
        Class cl11 = auto12.getClass();
        Class cl12 = cl11.getSuperclass();
        Method met11 = cl12.getDeclaredMethod("Move");
        met11.setAccessible(true);
        Assert.assertFalse((Boolean) met11.invoke(auto12));


    }

    @Test
    public void TestDirFileCreation() {
        File file = new File("D:\\TestLab7/Serialize.ser");
        Assert.assertTrue(file.exists());

    }

    @Test
    public void TestSerializeDeserialize() throws IOException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        double acc1 = 5.5;
        int s1 = 2;
        AutoChild car2 = new AutoChild(acc1, s1);
        Class cl1 = car2.getClass();
        Class cl2 = cl1.getSuperclass();
        Field acc2 = cl2.getDeclaredField("acceleration");
        acc2.setAccessible(true);
        Field s2 = cl2.getDeclaredField("s");
        s2.setAccessible(true);
        Main.MySerialize(car2);
        acc2.setDouble(car2, 0);
        s2.setInt(car2, 0);
        car2 = Main.MyDeserialize();
        double accValue2 = acc2.getDouble(car2);
        int sValue2 = s2.getInt(car2);
        Assert.assertTrue(accValue2 == acc1);
        Assert.assertTrue(sValue2 == s1);


    }
}

