import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class TestRozetka {

    private static WebDriver driver;
    private static String baseUrl;
    private static int counter;

    @BeforeClass
    public static void setUp()throws Exception{
        driver = new FirefoxDriver();
        baseUrl = "http://rozetka.com.ua/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        counter = 0;
    }

    @Before
    public void setBeforeEachTest()throws Exception {
        driver.get(baseUrl);
        driver.manage().window().maximize();


            }

    @After
    public void doAfterEachTest() throws Exception{
        counter++;
        System.out.println(counter+"tests have been run");
    }

    @AfterClass
    public static void tearDown()throws Exception{
        driver.quit();
    }

    @Test
    public void testLogoDisplaying()throws Exception{
        Assert.assertTrue("Logo is not displayed",
                driver.findElement(By.xpath(".//*[@id='body-header']/descendant::div[@class='logo']/img")).isDisplayed());
    }

    @Test
    public void testAppleIsInTheCatalog()throws Exception{
        Assert.assertTrue("Apple is not in the catalog",
                !driver.findElements(By.xpath(".//*[@id='m-main']/li/a[contains(text(),'Apple')]")).isEmpty()&&
                        driver.findElement(By.xpath(".//*[@id='m-main']/li/a[contains(text(),'Apple')]")).isDisplayed());
    }

    @Test
    public void testSectionWithMP3IsInTheCatalog()throws Exception{
        Assert.assertTrue("Apple is not in the catalog",
                !driver.findElements(By.xpath(".//*[@id='m-main']/li/a[contains(text(),'MP3')]")).isEmpty()&&
                        driver.findElement(By.xpath(".//*[@id='m-main']/li/a[contains(text(),'MP3')]")).isDisplayed());
    }

    @Test
    public void testCities()throws Exception{
        driver.findElement(By.xpath(".//*[@id='body-header']/descendant::div[@id='city-chooser']")).click();
        Assert.assertTrue("Киев is not displayed",
                !driver.findElements(By.xpath(".//*[@id='city-chooser']/descendant::*[@locality_id='1']")).isEmpty()&&
                    driver.findElement(By.xpath(".//*[@id='city-chooser']/descendant::*[@locality_id='1']")).isDisplayed());
        Assert.assertTrue("Одесса is not displayed",
                !driver.findElements(By.xpath(".//*[@id='city-chooser']/descendant::*[@locality_id='30']")).isEmpty()&&
                        driver.findElement(By.xpath(".//*[@id='city-chooser']/descendant::*[@locality_id='30']")).isDisplayed());
        Assert.assertTrue("Харьков is not displayed",
                !driver.findElements(By.xpath(".//*[@id='city-chooser']/descendant::*[@locality_id='31']")).isEmpty()&&
                        driver.findElement(By.xpath(".//*[@id='city-chooser']/descendant::*[@locality_id='31']")).isDisplayed());

    }

    @Test
    public void testCartIsEmpty() throws Exception{
        driver.findElement(By.xpath(".//ul[@class='header-user-buttons']/li[@id='cart_popup_header']")).click();
        Assert.assertTrue("Cart is not empty",
                !driver.findElements(By.xpath(".//*[@id='drop-block']/h2[contains(@class,'empty-cart-title inline')]")).isEmpty()&&
                        driver.findElement(By.xpath(".//*[@id='drop-block']/h2[contains(@class,'empty-cart-title inline')]")).isDisplayed());

    }
}
