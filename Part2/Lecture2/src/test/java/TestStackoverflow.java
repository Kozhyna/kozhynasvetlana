import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TestStackoverflow {
        private static WebDriver driver;
        private static String baseUrl;
        private static int counter;

        @BeforeClass
        public static void setUp()throws Exception{
            driver = new FirefoxDriver();
            baseUrl = "http://stackoverflow.com/";
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            counter=0;

        }

        @Before
        public void setBeforeEachTest()throws Exception {
            driver.get(baseUrl);
            driver.manage().window().maximize();

        }

        @After
        public void doAfterEachTest() throws Exception{
            counter++;
            System.out.println(counter+" tests have been run");

        }

        @AfterClass
        public static void tearDown()throws Exception{
            driver.quit();
        }

    @Test
    public void testFeatured()throws Exception{
        Assert.assertTrue("Featured is less than or equal to 300",
                Integer.parseInt(driver.findElement(By.xpath(".//*[@id='tabs']//span[@class='bounty-indicator-tab']")).getText()) > 300);
    }

    @Test
    public void testSignUpPage()throws Exception{
        driver.findElement(By.xpath(".//div[@class='topbar']//div[@class='topbar-links']//a[contains(text(),'sign up')]")).click();
        Assert.assertTrue("Google button is not displayed on SignUp page",
                !driver.findElements(By.xpath(".//*[@id='openid-buttons']/div/div[@class='text']/span[text()='Google']")).isEmpty()&&
                        driver.findElement(By.xpath(".//*[@id='openid-buttons']/div/div[@class='text']/span[text()='Google']")).isDisplayed());

        Assert.assertTrue("Facebook button is not displayed on SignUp page",
                !driver.findElements(By.xpath(".//*[@id='openid-buttons']/div/div[@class='text']/span[text()='Facebook']")).isEmpty()&&
                        driver.findElement(By.xpath(".//*[@id='openid-buttons']/div/div[@class='text']/span[text()='Facebook']")).isDisplayed());

    }

    @Test
    public void testTopQuestionAskedToday()throws Exception{
        Random rand = new Random();
        int tmp = rand.nextInt(96)+1;
        String tmpPath = ".//*[@id='question-mini-list']/div["+tmp+"]//h3/a";
        driver.findElement(By.xpath(tmpPath)).click();
        Assert.assertTrue("Question is posted not today",
                driver.findElement(By.xpath(".//*[@id='qinfo']/tbody/tr[1]/td[@style='padding-left: 10px']//b")).getText().equals("today"));




    }
    }


