package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Random;


public class RozetkaMainPage

{

    private WebDriver driver;

    public RozetkaMainPage(WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver = driver;

    }


    @FindBy(xpath = ".//*[@id='body-header']/descendant::div[@class='logo']/img")
    public WebElement logo;

    @FindBy(xpath = ".//*[@id='m-main']/li/a[contains(text(),'Apple')]")
    public WebElement apple_in_catalog;

    @FindBy(xpath = ".//*[@id='m-main']/li/a[contains(text(),'MP3')]")
    public WebElement mp3_in_catalog;

    @FindBy(xpath = ".//*[@id='SubscribePushNotificationPanel']/div/div[@class='notificationPanelCross']")
    public WebElement notif_panel_cross;

    @FindBy(xpath = ".//*[@id='city-chooser']/a/span")
    public WebElement city_chooser;

    @FindBy(xpath = ".//*[@id='city-chooser']/descendant::*[@locality_id='1']")
    public WebElement city_Kiev;

    @FindBy(xpath = ".//*[@id='city-chooser']/descendant::*[@locality_id='30']")
    public WebElement city_Odessa;

    @FindBy(xpath = ".//*[@id='city-chooser']/descendant::*[@locality_id='31']")
    public WebElement city_Kharkov;

    @FindBy(xpath = ".//ul[@class='header-user-buttons']/li[@id='cart_popup_header']")
    public WebElement cart_link;

    @FindBy(xpath = ".//*[@id='drop-block']/h2[contains(@class,'empty-cart-title inline')]")
    public WebElement cart_is_empty_text;


    public void openCityChooser()
    {
        notif_panel_cross.click();
        city_chooser.click();
    }

    public void openCart()
    {
        cart_link.click();
    }

}
