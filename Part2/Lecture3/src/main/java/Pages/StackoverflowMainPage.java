package Pages;

import javafx.scene.web.WebView;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Random;
import java.util.concurrent.TimeUnit;


public class StackoverflowMainPage
{
    private WebDriver driver;


    public StackoverflowMainPage(WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver = driver;
        Random rand = new Random();
        int tmp = rand.nextInt(96)+1;
        top_question_link = driver.findElement(By.xpath(".//*[@id='question-mini-list']/div["+tmp+"]//h3/a"));

    }

    public WebElement top_question_link;

    @FindBy(xpath = ".//*[@id='tabs']//span[@class='bounty-indicator-tab']")
    public WebElement featured;

    @FindBy(xpath = ".//div[@class='topbar']//div[@class='topbar-links']//a[contains(text(),'sign up')]")
    public WebElement signup_link;


    public int getFeaturedNumber()
    {

        return Integer.parseInt(featured.getText());

    }


    public StackoverflowSignUpPage goToSignUp() throws InterruptedException
    {
        signup_link.click();
        TimeUnit.SECONDS.sleep(3);
        return new StackoverflowSignUpPage(driver);

    }


    public StackoverflowTopQuestionPage goToTopQuestion() throws InterruptedException
    {

        top_question_link.click();
        TimeUnit.SECONDS.sleep(3);
        return new StackoverflowTopQuestionPage(driver);

    }

}
