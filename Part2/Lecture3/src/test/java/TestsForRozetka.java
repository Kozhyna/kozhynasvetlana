import Pages.RozetkaMainPage;
import Pages.StackoverflowMainPage;
import Pages.StackoverflowSignUpPage;
import Pages.StackoverflowTopQuestionPage;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;


public class TestsForRozetka

{

    private static WebDriver driver;
    private static String baseUrl;
    protected RozetkaMainPage rozetkaMainPage;


    @BeforeClass
    public static void setUp()throws Exception
    {
        driver = new FirefoxDriver();
        baseUrl = "http://rozetka.com.ua/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


    }

    @Before
    public void setBeforeEachTest()throws Exception
    {
       if(driver == null)
        {
            driver = new FirefoxDriver();
        }
        driver.get(baseUrl);
        driver.manage().window().maximize();
        rozetkaMainPage = new RozetkaMainPage(driver);
    }


    @AfterClass
    public static void tearDown()throws Exception
    {
        driver.quit();
    }

    @Test
    public void testLogoDisplaying()
    {
        Assert.assertTrue("Logo is not displayed",
                rozetkaMainPage.logo.isDisplayed());
    }

    @Test
    public void testAppleIsInTheCatalog()
    {
        Assert.assertTrue("Apple is not in the catalog",
                rozetkaMainPage.apple_in_catalog.isDisplayed());
    }

    @Test
    public void testSectionWithMP3IsInTheCatalog()
    {
        Assert.assertTrue("Apple is not in the catalog",
                rozetkaMainPage.mp3_in_catalog.isDisplayed());
    }

    @Test
    public void testCities()
    {
        rozetkaMainPage.openCityChooser();
        Assert.assertTrue("Киев is not displayed",
                rozetkaMainPage.city_Kiev.isDisplayed());
        Assert.assertTrue("Одесса is not displayed",
                rozetkaMainPage.city_Odessa.isDisplayed());
        Assert.assertTrue("Харьков is not displayed",
                rozetkaMainPage.city_Kharkov.isDisplayed());

    }

    @Test
    public void testCartIsEmpty()
    {
        rozetkaMainPage.openCart();
        Assert.assertTrue("Cart is not empty",
                rozetkaMainPage.cart_is_empty_text.isDisplayed());

    }
}
