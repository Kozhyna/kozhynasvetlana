import Pages.StackoverflowMainPage;
import Pages.StackoverflowSignUpPage;
import Pages.StackoverflowTopQuestionPage;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TestsForStackoverflow

{

    private static WebDriver driver;
    private static String baseUrl;
    protected StackoverflowMainPage mainPage;
    protected StackoverflowSignUpPage signUpPage;
    protected StackoverflowTopQuestionPage topQuestionPage;

    @BeforeClass
    public static void setUp()throws Exception
    {
        driver = new FirefoxDriver();
        baseUrl = "http://stackoverflow.com/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


    }

    @Before
    public void setBeforeEachTest()throws Exception
    {
        if(driver == null)
        {
            driver = new FirefoxDriver();
        }
        driver.get(baseUrl);
        driver.manage().window().maximize();
        mainPage = new StackoverflowMainPage(driver);
    }


    @AfterClass
    public static void tearDown()throws Exception
    {
        driver.quit();
    }


    @Test
    public void testFeatured()
    {

        Assert.assertTrue("Featured is less than or equal to 300", mainPage.getFeaturedNumber() > 300);

    }

    @Test
    public void testSignUpPage() throws InterruptedException
    {
        signUpPage = mainPage.goToSignUp();
        Assert.assertTrue("Google button is not displayed on SignUp page",
                signUpPage.google_button.isDisplayed());

        Assert.assertTrue("Facebook button is not displayed on SignUp page",
                signUpPage.facebook_button.isDisplayed());
    }

    @Test
    public void testTopQuestionAskedToday() throws InterruptedException
    {

        topQuestionPage = mainPage.goToTopQuestion();
        Assert.assertTrue("Question is posted not today",
                topQuestionPage.getAskedText().equals("today"));




    }
}
