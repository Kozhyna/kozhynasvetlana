@rozetka
 Feature: test Rozetka

  Background:
    Given the user is on main page

@2
  Scenario: 001 check logo displaying on the main Rozetka page
    Given I on Rozetka main page
    Then I see Rozetka logo

@2
  Scenario: 002 check Apple is in the catalog menu
    Given I on Rozetka main page
    Then I see Apple in the catalog menu

  Scenario: 003 check Apple is in the catalog menu
    Given I on Rozetka main page
    Then I see MPЗ in the catalog menu

  Scenario: 004 check cities Kiev, Kharkov, Odessa are in the list
    Given I on Rozetka main page
    When I click on City link
    Then I see Kiev, Kharkov, Odessa in the cities list

  Scenario: 005 check cart is empty
    Given I on Rozetka main page
    When I click on Cart link
    Then I see on the page text that cart is empty