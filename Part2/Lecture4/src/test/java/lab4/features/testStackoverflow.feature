@stackoverflow
Feature: test Stackoverflow

  Background:
    Given the user is on main page of Stackoverflow

  Scenario: 001 check featured number is greater than 300
    Given I on Stackoverflow main page
    Then I see featured number greater than 300

@2
  Scenario: 002 check Google and Facebook buttons displaying on Sign up page
    When I click sign up
    Then I see Sign up page with Google and Facebook buttons

@2
  Scenario: 003 check any Top Question is asked today
    Given I on Stackoverflow main page
    When I click any Top Question
    Then I see date asked is today