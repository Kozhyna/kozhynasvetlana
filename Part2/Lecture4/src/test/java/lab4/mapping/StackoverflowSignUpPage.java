package lab4.mapping;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StackoverflowSignUpPage

{

        private WebDriver driver;

        public StackoverflowSignUpPage(WebDriver driver)
        {
            PageFactory.initElements(driver,this);
            this.driver = driver;
        }

        @FindBy(xpath = ".//*[@id='openid-buttons']/div/div[@class='text']/span[text()='Google']")
        public WebElement google_button;

        @FindBy(xpath = ".//*[@id='openid-buttons']/div/div[@class='text']/span[text()='Facebook']")
        public WebElement facebook_button;




}


