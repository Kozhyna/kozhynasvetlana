package lab4.mapping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StackoverflowTopQuestionPage

{
    private WebDriver driver;


    public StackoverflowTopQuestionPage(WebDriver driver)
    {
        PageFactory.initElements(driver,this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='qinfo']/tbody/tr[1]/td[@style='padding-left: 10px']//b")
    public WebElement asked;

    public String getAskedText()
    {

        return asked.getText();

    }
}
