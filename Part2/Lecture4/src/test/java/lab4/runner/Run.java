package lab4.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import lab4.mapping.RozetkaMainPage;
import lab4.mapping.StackoverflowMainPage;
import lab4.mapping.StackoverflowSignUpPage;
import lab4.mapping.StackoverflowTopQuestionPage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/lab4/features",
        glue = "lab4/steps",
        tags = "@rozetka, @stackoverflow"  //1)чтоб запустились все
        // tags="@2"                         2)чтоб запустились по 2 теста для розетки и 2 для стековерфлоу
        //tags = "@rozetka"                  3)чтоб запустились только тесты розетки
        //tags = "@stackoverflow"            4)чтоб запустились только тесты стековерфлоу
)
public class Run
{
    public static WebDriver driver;



    @BeforeClass
    public static void setUp()throws Exception
    {
        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void tearDown()throws Exception
    {
        driver.quit();
    }
}