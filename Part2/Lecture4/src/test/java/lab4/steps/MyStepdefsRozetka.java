package lab4.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab4.mapping.RozetkaMainPage;
import org.junit.Assert;

import java.util.concurrent.TimeUnit;

import static lab4.runner.Run.driver;


public class MyStepdefsRozetka {


    public RozetkaMainPage rozetkaMainPage;

    @Given("^I on Rozetka main page$")
    public void iOnRozetkaMainPage() throws Throwable
    {
        if(!driver.getTitle().equals("Интернет-магазин ROZETKA™: фототехника, видеотехника, аудиотехника, компьютеры и компьютерные комплектующие"))
            driver.get("http://rozetka.com.ua/");
    }


    @Then("^I see Rozetka logo$")
    public void iSeeRozetkaLogo() throws Throwable
    {
        Assert.assertTrue("Logo is not displayed",
                rozetkaMainPage.logo.isDisplayed());
    }


    @Then("^I see Apple in the catalog menu$")
    public void iSeeAppleInTheCatalogMenu() throws Throwable
    {
        Assert.assertTrue("Apple is not in the catalog",
                rozetkaMainPage.apple_in_catalog.isDisplayed());
    }

    @Then("^I see MPЗ in the catalog menu$")
    public void iSeeMPЗInTheCatalogMenu() throws Throwable
    {
        Assert.assertTrue("Apple is not in the catalog",
                rozetkaMainPage.mp3_in_catalog.isDisplayed());
    }


    @When("^I click on City link$")
    public void iClickOnCityLink() throws Throwable
    {
        rozetkaMainPage.openCityChooser();
    }

    @Then("^I see Kiev, Kharkov, Odessa in the cities list$")
    public void iSeeKievKharkovOdessaInTheCitiesList() throws Throwable
    {
        Assert.assertTrue("Киев is not displayed",
                rozetkaMainPage.city_Kiev.isDisplayed());
        Assert.assertTrue("Одесса is not displayed",
                rozetkaMainPage.city_Odessa.isDisplayed());
        Assert.assertTrue("Харьков is not displayed",
                rozetkaMainPage.city_Kharkov.isDisplayed());
    }

    @When("^I click on Cart link$")
    public void iClickOnCartLink() throws Throwable
    {
        rozetkaMainPage.openCart();
    }

    @Then("^I see on the page text that cart is empty$")
    public void iSeeOnThePageTextThatCartIsEmpty() throws Throwable
    {
        Assert.assertTrue("Cart is not empty",
                rozetkaMainPage.cart_is_empty_text.isDisplayed());
    }


    @Given("^the user is on main page$")
    public void theUserIsOnMainPage() throws Throwable
    {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        if (!driver.getCurrentUrl().equals("http://rozetka.com.ua/"))
            driver.get("http://rozetka.com.ua/");
            rozetkaMainPage = new RozetkaMainPage(driver);
    }
}
