package lab4.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab4.mapping.StackoverflowMainPage;
import lab4.mapping.StackoverflowSignUpPage;
import lab4.mapping.StackoverflowTopQuestionPage;
import org.junit.Assert;

import java.util.concurrent.TimeUnit;

import static lab4.runner.Run.driver;


public class MyStepdefsStackoverflow
{
    public  StackoverflowMainPage mainPage;
    public  StackoverflowSignUpPage signUpPage;
    public  StackoverflowTopQuestionPage topQuestionPage;

    @Given("^I on Stackoverflow main page$")
    public void iOnStackoverflowMainPage() throws Throwable
    {
        if(!driver.getTitle().equals("Stack Overflow"))
            driver.get("http://stackoverflow.com/");
    }

    @Then("^I see featured number greater than (\\d+)$")
    public void iSeeFeaturedNumberGreaterThan(int arg) throws Throwable
    {

        Assert.assertTrue("Featured is less than or equal to 300", mainPage.getFeaturedNumber() > arg);
    }

    @When("^I click sign up$")
    public void iClickSignUp() throws Throwable
    {
        signUpPage = mainPage.goToSignUp();
    }

    @Then("^I see Sign up page with Google and Facebook buttons$")
    public void iSeeSignUpPageWithGoogleAndFacebookButtons() throws Throwable
    {
        Assert.assertTrue("Google button is not displayed on SignUp page",
                signUpPage.google_button.isDisplayed());

        Assert.assertTrue("Facebook button is not displayed on SignUp page",
                signUpPage.facebook_button.isDisplayed());
    }

    @When("^I click any Top Question$")
    public void iClickAnyTopQuestion() throws Throwable
    {
        mainPage = new StackoverflowMainPage(driver);
        topQuestionPage = mainPage.goToTopQuestion();
    }

    @Then("^I see date asked is today$")
    public void iSeeDateAskedIsToday() throws Throwable {
        Assert.assertTrue("Question is posted not today",
                topQuestionPage.getAskedText().equals("today"));
    }

    @Given("^the user is on main page of Stackoverflow$")
    public void theUserIsOnMainPageOfStackoverflow() throws Throwable {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (!driver.getCurrentUrl().equals("http://stackoverflow.com/"))
            driver.get("http://stackoverflow.com/");
        driver.manage().window().maximize();
        mainPage = new StackoverflowMainPage(driver);
    }
}
